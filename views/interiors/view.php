<?php

use yii\widgets\Breadcrumbs;

$this->title = 'Каталог';
?>
<section class="page">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="/">Главная</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl('interiors'); ?>">Галерея</a></li>
            <li><a><?= $item->name ?></a></li>
        </ul>
        <div class="row page--wrapper">
            <div class="col-lg-8 col-md-7 col-xs-12 page--wrapper-gallery-left">
                <div class="page--wrapper-gallery-left--slider">
                    <ul id="imageGallery">
                        <?php foreach ($item->getImages() as $image): ?>
                            <li data-thumb="<?= $image->getUrl('200x100'); ?>" data-src="<?= $image->getUrl('300x105'); ?>">
                                <img src="<?= $image->getUrl('300x100'); ?>" />
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="page--wrapper-gallery-left--description">
                    <?= $item->description; ?>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 col-xs-12 page--wrapper-gallery-right">
                <div class="page--wrapper-gallery-right--title">
                    <h1><?= $item->name; ?></h1>
                </div>
                <div class="page--wrapper-gallery-right--info">
                    <ul>
                        <li style="background: #f4f4f4; padding: 20px; margin-bottom: 18px;">
                            <p style="font-size: 22px;">Стоимость с мебелью и аксессуарами:</p>
                            <span style="font-size: 29px; font-weight: bold; color: #d51f2f;"><?= $item->total_cost; ?> тг/м2</span>
                        </li>

                        <li>
                            <p class="info-price">Площадь объекта:</p>
                            <span class="info-price"><?= $item->square; ?> m2</span>
                        </li>
                        <li>
                            <p class="info-price">Общая стоимость проекта:</p>
                            <span class="info-price"><?= $item->projects_cost; ?> тенге</span>
                        </li>
                        <li>
                            <p class="info-price">Стоимость ремонтных работ:</p>
                            <span class="info-price"><?= $item->renovations_cost; ?> тенге</span>
                        </li>
                        <li>
                            <p class="info-price">Стоимость отделочных материалов:</p>
                            <span class="info-price"><?= $item->decorations_cost; ?> тенге</span>
                        </li>
                        <li>
                            <p class="info-price">Стоимость мебели и аксессуаров:</p>
                            <span class="info-price"><?= $item->furniture_cost; ?> тенге</span>
                        </li>
                    </ul>
                </div>
                <div class="page--wrapper-gallery-right--price">
                    <h2>Спецификация</h2>
                    <div class="tabs">
                        <ul class="tabs__caption">
                            <li class="active">Общая стоимость</li>
                            <li>Мебель и аксессуары</li>
                            <li>Отделочные материалы</li>
                            <li>Ремонтные работы</li>
                        </ul>
                        <div class="tabs__content mCustomScrollbar active" data-mcs-theme="dark" >
                            <?php foreach ($item->allRelated as $related): ?>
                                <article class="tabs__content_items">
                                    <div class="tabs__content_item__left">
                                        <?php if ($image = $related->getImage()): ?>
                                            <div class="tabs__content_item__left_img">
                                                <img src="<?= $image->getUrl('50x50'); ?>" alt="">
                                            </div>
                                        <?php endif; ?>

                                        <div class="tabs__content_item__left_description">
                                            <p><?= $related->name; ?></p>
                                            <span><?= $related->description; ?></span>
                                        </div>
                                    </div>
                                    <div class="tabs__content_item__right_price">
                                        <?= $related->total_cost ?> тг
                                    </div>
                                </article>
                            <?php endforeach; ?>

                        </div>
                        <div class="tabs__content">
                            <?php foreach ($item->furniture as $furniture): ?>
                                <article class="tabs__content_items">
                                    <div class="tabs__content_item__left">
                                        <div class="tabs__content_item__left_img">
                                            <img class = "img-responsive" src=<?= $furniture->getImage()->getUrl("50x50"); ?> alt="">
                                        </div>
                                        <div class="tabs__content_item__left_description">
                                            <p><?= $furniture->name; ?></p>
                                            <span><?= $furniture->description; ?></span>
                                        </div>
                                    </div>
                                    <div class="tabs__content_item__right_price">
                                        <?= $furniture->total_cost; ?> тг
                                    </div>
                                </article>
                            <?php endforeach; ?>
                        </div>
                        <div class="tabs__content">
                            <?php foreach ($item->decorations as $decoration): ?>
                                <article class="tabs__content_items">
                                    <div class="tabs__content_item__left">
                                        <div class="tabs__content_item__left_img">
                                            <img src="<?= $decoration->getImage()->getUrl("50x50"); ?>" alt="">
                                        </div>
                                        <div class="tabs__content_item__left_description">
                                            <p><?= $decoration->name ?></p>
                                        </div>
                                    </div>
                                    <div class="tabs__content_item__right_price">
                                        <?= $decoration->total_cost; ?> тг
                                    </div>
                                </article>
                            <?php endforeach; ?>

                        </div>
                        <div class="tabs__content">
                            <?php foreach ($item->renovations as $renovation): ?>
                                <article class="tabs__content_items">
                                    <div class="tabs__content_item__left">
                                        <div class="tabs__content_item__left_description">
                                            <p><?= $renovation->name ?></p>
                                        </div>
                                    </div>
                                    <div class="tabs__content_item__right_price">
                                        <?= $renovation->total_cost; ?> тг
                                    </div>
                                </article>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>

                <div class="page--wrapper-gallery-right--total">
                    <div class="right--total-price">
                        <p>Итого:</p>

                        <span><?= $item->amount; ?> тг</span>
                    </div>
                    <div class="right--total-button">
                        <button href="#call-consult" class="total-button--modal button-consult">Хочу так же</button>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 page--wrapper-gallery-more">
                <div class="page--wrapper-gallery-more--title">
                    <h3>Обратите внимание</h3>
                </div>
                <div class="page--wrapper-right-items-gallery">
                    <article class="gallery-item gallery-item-full-page">
                        <div class="gallery-item--img">
                            <a href="img/gallery/gl1.jpg" data-lightbox="roadtrip-8"><img src="img/gallery/gl1.jpg" /></a>
                            <p>65 000 тг/м2</p>
                            <a href="" class="galery-cursor"></a>
                        </div>
                        <div class="gallery-item--title">
                            <a href="">Дом с барельефами и витражами</a>
                            <div class="tags hidden">
                                <a href="">Современный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                        </div>
                    </article>
                    <article class="gallery-item gallery-item-full-page">
                        <div class="gallery-item--img">
                            <a href="img/gallery/gl1.jpg" data-lightbox="roadtrip-8"><img src="img/gallery/gl1.jpg" /></a>
                            <p>65 000 тг/м2</p>
                            <a href="" class="galery-cursor"></a>
                        </div>
                        <div class="gallery-item--title">
                            <a href="">Дом с барельефами и витражами</a>
                            <div class="tags hidden">
                                <a href="">Современный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                        </div>
                    </article>
                    <article class="gallery-item gallery-item-full-page">
                        <div class="gallery-item--img">
                            <a href="img/gallery/gl1.jpg" data-lightbox="roadtrip-8"><img src="img/gallery/gl1.jpg" /></a>
                            <p>65 000 тг/м2</p>
                            <a href="" class="galery-cursor"></a>
                        </div>
                        <div class="gallery-item--title">
                            <a href="">Дом с барельефами и витражами</a>
                            <div class="tags hidden">
                                <a href="">Современный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
