<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">

    <title><?= $this->title; ?></title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Template Basic Images Start -->
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
    <!-- Template Basic Images End -->

    <?php $this->head() ?>

    <!-- Custom Browsers Color Start -->
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <!-- Custom Browsers Color End -->
    <!-- Custom Browsers Color End -->



</head>

<body>
<?php $this->beginBody() ?>

<?php
// NavBar::begin([
//     'brandLabel' => 'My Company',
//     'brandUrl' => Yii::$app->homeUrl,
//     'options' => [
//         'class' => 'navbar-inverse navbar-fixed-top',
//     ],
// ]);
// echo Nav::widget([
//     'options' => ['class' => 'navbar-nav navbar-right'],
//     'items' => [
//         ['label' => 'Home', 'url' => ['/site/index']],
//         ['label' => 'About', 'url' => ['/site/about']],
//         ['label' => 'Contact', 'url' => ['/site/contact']],
//         Yii::$app->user->isGuest ? (
//             ['label' => 'Login', 'url' => ['/site/login']]
//         ) : (
//             '<li>'
//             . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
//             . Html::submitButton(
//                 'Logout (' . Yii::$app->user->identity->username . ')',
//                 ['class' => 'btn btn-link']
//             )
//             . Html::endForm()
//             . '</li>'
//         )
//     ],
// ]);
// NavBar::end();
?>

<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>




<div id="myNav" class="overlay">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <div class="overlay-content">
        <a href="<?= Yii::$app->urlManager->createUrl(['gallery/index']); ?>">Галерея интерьеров</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['catalog/index']); ?>">Каталог товаров</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['site/about']); ?>">О нас</a>
        <a href="<?= Yii::$app->urlManager->createUrl(['site/contacts']); ?>">Контакты</a>
    </div>
</div>

<header>
    <div class="container">
        <nav>
            <div class="header-left">
                <a href="<?= Yii::$app->urlManager->createUrl(['site/index']); ?>">
                    <img src="img/logo.png" alt="" class="img-responsive logo">
                </a>
            </div>
            <div class="header-right">
                <div class="header-right--social-menu">
                    <div class="header-right--social clearfix">
                        <ul class="social">
                            <li>
                                <a href="">
                                    <i class="icon-vk"></i>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="icon-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <i class="icon-odnaklasniki"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="header-right--menu clearfix">
                        <ul>
                            <li><a href="<?= Yii::$app->urlManager->createUrl(['gallery/index']); ?>">Галерея интерьеров</a></li>
                            <li><a href="<?= Yii::$app->urlManager->createUrl(['catalog/index']); ?>">Каталог товаров</a></li>
                            <li><a href="<?= Yii::$app->urlManager->createUrl(['site/about']); ?>">О нас</a></li>
                            <li><a href="<?= Yii::$app->urlManager->createUrl(['site/contacts']); ?>">Контакты</a></li>
                        </ul>
                    </div>
                </div>
                <div class="header-right--call">
                    <p>
                        <i class="icon-phone"></i>
                        +7 800 123 45 67
                    </p>
                    <button href="#call-request" class="call-button">Обратная связь</button>
                </div>
            </div>
        </nav>
    </div>
    <div class="nav-bottom visible-xs-block">
        <div class="container">
            <div class="nav-bottom-wrapper">
                <ul class="social">
                    <li>
                        <a href="">
                            <i class="icon-vk"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="icon-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="icon-odnaklasniki"></i>
                        </a>
                    </li>
                </ul>
                <a class="toggle-button visible-xs" onclick="openNav()"><span></span><span></span><span></span></a>
            </div>
        </div>
    </div>
</header>

<?= $content ?>

<footer class="footer">
    <div class="container">
        <div class="footer--wrapper">
            <div class="footer--wrapper-left">
                <ul class="footer--wrapper-left--menu">
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['catalog/index']); ?>">Каталог товаров</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['catalog/index']); ?>">Галерея интерьеров</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['site/about']); ?>">О нас</a></li>
                    <li><a href="<?= Yii::$app->urlManager->createUrl(['site/contacts']); ?>">Контакты</a></li>
                </ul>
                <div class="footer--wrapper-left--copyright">
                    <span class="red-info">Стройтека</span>
                    <span>| Все права защищены</span>
                </div>
            </div>
            <div class="footer--wrapper-right">
                <ul class="social social-footer">
                    <li>
                        <a href="">
                            <i class="icon-vk"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="icon-facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="icon-odnaklasniki"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
                <div class="footer--wrapper-right--copyright">
                    <span class="red-info">Создание сайта</span>
                    <span>| Creatie Design</span>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Call Modal -->
<div id="call-request" class="mfp-hide call-request">
    <div class="call-request--title">
        <h1>Заказать звонок</h1>
    </div>
    <div class="call-request--content">
        <form action="">
            <div class="form-label">
                <label for="name">Введите Ваши имя:</label>
                <div class="form-label--input-name">
                    <input type="text" id="name" name="name" placeholder="Иван Иванов" required>
                </div>
            </div>
            <div class="form-label">
                <label for="phone">Введите Ваш телефон:</label>
                <div class="form-label--input-phone">
                    <input type="tel" id="phone" class="phone" name="name" placeholder="+7(___)___-____" required>
                </div>
            </div>
            <div class="form-label">
                <button type="submit" class="form-call-button">Заказать звонок</button>
            </div>
            <p>
                Ваши данные не будут переданны третьим лицам!
            </p>
        </form>
    </div>
</div>


<!-- Call Consult -->
<div id="call-consult" class="mfp-hide call-request">
    <div class="call-request--title">
        <h1>Заказать выезд
            консультанта</h1>
    </div>
    <div class="call-request--content">
        <form action="">
            <div class="form-label">
                <label for="name">Введите Ваши имя:</label>
                <div class="form-label--input-name">
                    <input type="text" id="name" name="name" placeholder="Иван Иванов" required>
                </div>
            </div>
            <div class="form-label">
                <label for="phone">Введите Ваш телефон:</label>
                <div class="form-label--input-phone">
                    <input type="tel" id="phone" class="phone" name="name" placeholder="+7(___)___-____" required>
                </div>
            </div>
            <div class="form-label">
                <label for="email">Введите Ваш email:</label>
                <div class="form-label--input-mail">
                    <input type="email" id="email" name="email" placeholder="simple@mail.ru" required>
                </div>
            </div>
            <div class="form-label">
                <button type="submit" class="form-call-button">Заказать выезд</button>
            </div>
            <p>
                Ваши данные не будут переданны третьим лицам!
            </p>
        </form>
    </div>
</div>

<!-- Buy Modal-->
<div id="buy-modal" class="mfp-hide buy-modal">
    <div class="buy-modal--title">
        <h1>Оформление заказа</h1>
    </div>
    <div class="buy-modal--content">
        <form action="">
            <div class="form-label">
                <div class="form-label--input-name">
                    <input type="text" id="name" name="name" placeholder="Иван Иванов" required>
                </div>
            </div>
            <div class="form-label">
                <div class="form-label--input-phone">
                    <input type="tel" id="phone" class="phone" name="name" placeholder="+7(___)___-____" required>
                </div>
            </div>
            <div class="form-label">
                <div class="form-label--input-mail">
                    <input type="email" id="email" name="email" placeholder="simple@mail.ru" required>
                </div>
            </div>
            <div class="form-label">
                <button type="submit" class="form-call-button">Отправить</button>
            </div>
            <p>
                С вами свяжется наш специалист в
                течении 30 минут и уточнит
                параметры заказа!
            </p>
        </form>
    </div>
</div>

<?php $this->endBody() ?>
<script>
    function openNav() {
        document.getElementById("myNav").style.width = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.width = "0%";
    }
</script>
<script>
    jQuery(document).ready(function(){

        jQuery('.c_carousel').each(function(index){


            if(jQuery(".c_carousel").eq(index).length) {
                jQuery('.c_carousel').eq(index).flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    itemWidth: 50,
                    pausePlay: false,
                    asNavFor: '.c_slider:eq('+index+')',
                    itemMargin: 15,
                });
            }

            if(jQuery('.c_slider').eq(index).length){
                jQuery('.c_slider').eq(index).flexslider({
                    animation: "slide",
                    controlNav: false,
                    directionNav: false,
                    animationLoop: false,
                    slideshow: false,
                    pausePlay: false,
                    sync: ".c_carousel:eq("+index+")",

                });

            }

        })

    });
</script>

<!-- view template -->

<script>
    function make2Rows(iWidth) {
        var iHeight = parseFloat($('.flexslider .slides > li').height());
        $('.alliance-list .slides > li').css('width', iWidth+'px');
        $('.alliance-list .slides > li:nth-child(even)').css('margin', iHeight+'px 0px 0px -'+iWidth+'px');
    }
    // Can also be used with $(document).ready()
    $(window).load(function() {
        var itemCnt = 5; // this will be the number of columns per row
        var iWidth = parseFloat($('.flexslider').width() / itemCnt);
        // The slider being synced must be initialized first
        $('#carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 80,
            itemMargin: 25,
            directionNav: false,
            animationLoop: false,
            asNavFor: '#slider'
        });

        $('#slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: "#carousel"
        });
    });
</script>
<script>
    //  Custom croll-bar
    (function($){
        $(window).on("load",function(){
            $(".tabs__content").mCustomScrollbar();
        });
    })(jQuery);
</script>
<script>
    $(document).ready(function() {
        $('#imageGallery').lightSlider({
            gallery:true,
            item:1,
            loop:false,
            thumbItem:9,
            slideMargin:0,
            enableDrag: false,
            currentPagerPosition:'left',
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }
        });
    });
</script>
<script>
    (function ($) {
        $('.btn:first-of-type').on('click', function() {
            $('.page--wrapper-catalog-right--price-buy input').val( parseInt($('.page--wrapper-catalog-right--price-buy input').val(), 10) + 1);
        });
        $('.btn:last-of-type').on('click', function() {
            $('.page--wrapper-catalog-right--price-buy input').val( parseInt($('.page--wrapper-catalog-right--price-buy input').val(), 10) - 1);
        });
    })(jQuery);
</script>


</body>
</html>
<?php $this->endPage() ?>
