<?php

/* @var $this yii\web\View */

$this->title = 'Галерея';
?>

<section class="page">
    <div class="container">
        <ul class="breadcrumbs">
            <li><a href="<?= Yii::$app->urlManager->createUrl(['site/index']); ?>">Главная</a></li>
            <li><a href="<?= Yii::$app->urlManager->createUrl(['gallery/index']); ?>">Галерея</a></li>
        </ul>
        <div class="row page--wrapper">
            <!-- Фильтр для десктопа -->
            <div class="col-lg-2 col-md-3 col-xs-12 page--wrapper-left mob hidden">
                <div class="page--filter">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingMenuOne">
                                <a role="button" class="accordion-button collapsed-in" data-toggle="collapse"
                                   data-parent="#accordion" href="#collapseMenuOne" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    Стиль
                                </a>
                            </div>
                            <div id="collapseMenuOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingMenuOne">
                                <div class="panel-body">
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-1-sidebar" name="radio">
                                        <label for="chekbox-1-sidebar">Современный</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-2-sidebar" name="radio">
                                        <label for="chekbox-2-sidebar">Фьюжн</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-3-sidebar" name="radio">
                                        <label for="chekbox-3-sidebar">Модернизм</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-4-sidebar" name="radio">
                                        <label for="chekbox-4-sidebar">Классический</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-5-sidebar" name="radio">
                                        <label for="chekbox-5-sidebar">Восточный</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingMenuTwo">
                                <a role="button" class="accordion-button collapsed-in" data-toggle="collapse"
                                   data-parent="#accordion2" href="#collapseMenuTwo" aria-expanded="true"
                                   aria-controls="collapseMenuTwo">
                                    Цвет
                                </a>
                            </div>
                            <div id="collapseMenuTwo" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingMenuTwo">
                                <div class="panel-body">
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-6-sidebar" name="radio">
                                        <label for="chekbox-6-sidebar">Светлый</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-7-sidebar" name="radio">
                                        <label for="chekbox-7-sidebar">Темный</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-8-sidebar" name="radio">
                                        <label for="chekbox-8-sidebar">Комбинированный</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingMenuThree">
                                <a role="button" class="accordion-button collapsed-in" data-toggle="collapse"
                                   data-parent="#accordion3" href="#collapseMenuThree" aria-expanded="true"
                                   aria-controls="collapseMenuThree">
                                    Бюджет
                                </a>
                            </div>
                            <div id="collapseMenuThree" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingMenuThree">
                                <div class="panel-body">
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-9-sidebar" name="radio">
                                        <label for="chekbox-9-sidebar">Бюджетный</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-10-sidebar" name="radio">
                                        <label for="chekbox-10-sidebar">Средний</label>
                                    </div>
                                    <div class="group">
                                        <input type="checkbox" class="radio" id="chekbox-11-sidebar" name="radio">
                                        <label for="chekbox-11-sidebar">Люкс</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="page--filter-button">Применить</button>
                    </div>
                </div>
            </div>
            
            <!-- Следующий блок было col-lg-10 col-md-9 col-xs-12 -->
            <div class="col-lg-12 col-md-12 col-xs-12 page--wrapper-right">
                <div class="page--wrapper-right-title">
                    <h1>Галерея</h1>
                    <!-- <p>135 работ</p> -->
                </div>
                <div class="page--wrapper-right-description">
                    <!-- <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis accusamus architecto quam at
                        recusandae repellendus, asperiores delectus libero necessitatibus blanditiis, atque, tempore
                        minus aspernatur eaque cupiditate odio dolores quasi quae.
                    </p> -->
                </div>
                <div class="page--wrapper-right-items-gallery">
                    <?php foreach ($gallery as $item): ?>
                    <article class="gallery-item">
                        <div class="gallery-item--img">
                            <a href="img/gallery/gl1.jpg" data-lightbox="roadtrip-1">
                                <img src="img/gallery/gl1.jpg"/>
                            </a>
                            <p><?= $item->totalcost; ?> тг</p>
                            <a href="<?= Yii::$app->urlManager->createUrl(['gallery/view']); ?>" class="galery-cursor"></a>
                        </div>
                        <div class="gallery-item--title">
                            <a href="<?= Yii::$app->urlManager->createUrl(['gallery/view' , 'id' => $item->id]); ?>"><?= $item->title; ?></a>
                            <div class="tags hidden">
                                <a href="">Современный,</a>
                                <a href="">Модернизм,</a>
                                <a href="">Фьюжн</a>
                            </div>
                        </div>
                    </article>
                    <?php endforeach; ?>
                </div>
                <ul class="pagination clearfix hidden">
                    <li><a class="active" href="">1</a></li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li><a href="">6</a></li>
                    <li><a href="">7</a></li>
                    <li><a href="">8</a></li>
                    <li><a href="">9</a></li>
                    <li><a href="">10</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>