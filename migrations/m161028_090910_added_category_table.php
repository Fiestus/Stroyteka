<?php

use yii\db\Migration;

class m161028_090910_added_category_table extends Migration
{

    const TABLE_CATEGORY = 'tbl_categories';
    const TABLE_PRODUCTS = 'tbl_products';

    public function safeUp()
    {
        $this->createTable(self::TABLE_CATEGORY, [
            'id' => 'pk',
            'id_parent' => 'int(11)',
            'depth' => 'int(11)',
            'name' => 'varchar(45)',
        ]);

        $this->addColumn(self::TABLE_PRODUCTS, 'category_id', 'int(11)');

        $this->insert(self::TABLE_CATEGORY, [
            'id_parent' => 0,
            'depth' => 1,
            'name' => '../'
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_PRODUCTS, 'category_id');
        $this->dropTable(self::TABLE_CATEGORY);
    }
}
