<?php

use yii\db\Migration;

class m161027_111602_furniture_table extends Migration
{

    const TABLE_NAME = "tbl_furniture";
    const TABLE_RELATIONS = 'tbl_interiors_furniture';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => 'pk',
            'name' => 'varchar(45)',
            'description' => 'text',
            'total_cost' => 'int(11)',
            'discount_cost' => 'int(11)',
        ]);

        $this->createTable(self::TABLE_RELATIONS, [
            'id' => 'pk',
            'furniture_id' => 'int(11)',
            'interior_id' => 'int(11)',

        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        $this->dropTable(self::TABLE_RELATIONS);
    }
}
