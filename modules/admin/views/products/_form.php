<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Categories;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Catalog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="catalog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'available')->dropDownList([0 => 'Нет', 1 => 'Да']); ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->fileInput(); ?>

    <?php $images = $model->getImages(); ?>
    <?php foreach ($images as $image): ?>
        <img data-image-id = "<?= $image->id; ?>" src="<?= $image->getUrl('250x150'); ?>" alt="">
    <?php endforeach; ?>
    <br><br>

    <?= $form->field($model, 'category_id')->dropDownList(Categories::getList()); ?>

    <?= $form->field($model, 'total_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'qtn')->textInput() ?>

    <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style_type')->textInput(); ?>

    <?= $form->field($model, 'size')->textInput(); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJS(sprintf(
    "$('[data-image-id]').on('dblclick', function(){
        var object = $(this);
        $.get('%s?id=' + $(this).data('imageId'), function(data){
            if (data) {
                object.detach();
            }
        });
    })", Yii::$app->urlManager->createUrl(['admin/' . Yii::$app->controller->id . '/delete-image'])
), yii\web\View::POS_READY); ?>
