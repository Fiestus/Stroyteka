<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Catalog */

$this->title = 'Добавить товар';
$this->params['breadcrumbs'][] = ['label' => 'Каталог продукции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page">
	<br><br>
	<div class="container">
		<div class="catalog-create">

		    <h1><?= Html::encode($this->title) ?></h1>
			<br>
		    <?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>

		</div>
	</div>
	<br><br>
</section>
