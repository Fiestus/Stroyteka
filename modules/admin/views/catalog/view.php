<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Catalog */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Каталог продукции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page">
    <br><br>
    <div class="container">
        <div class="catalog-view">

            <h1><?= Html::encode($this->title) ?></h1>
            <br>
            <p>
                <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'title',
                    'description:ntext',
                    'image:ntext',
                    'price',
                    'qtn',
                    'article',
                ],
            ]) ?>
        </div>
    </div>
    <br><br>
</section>
