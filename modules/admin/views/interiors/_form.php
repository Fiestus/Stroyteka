<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\multiselect\MultiSelect;
use app\modules\admin\models\Furniture;
use app\modules\admin\models\Decorations;
use app\modules\admin\models\Renovations;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gallery-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alias')->textInput() ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(),[
        'editorOptions' => [
            'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ],
    ]); ?>

    <?= $form->field($model, 'total_cost')->textInput(['maxlength' => true]); ?>


    <?= $form->field($model, 'furniture')->widget(MultiSelect::className(), [
        'data' => Furniture::getList(),
        "options" => ['multiple'=>"multiple"],
    ]); ?>

    <?= $form->field($model, 'decorations')->widget(MultiSelect::className(), [
        'data' => Decorations::getList(),
        "options" => ['multiple'=>"multiple"],
    ]); ?>

    <?= $form->field($model, 'renovations')->widget(MultiSelect::className(), [
        'data' => Renovations::getList(),
        "options" => ['multiple'=>"multiple"],
    ]); ?>

    <?= $form->field($model, 'image[]')->fileInput(['multiple' => true]); ?>
    <?php $images = $model->getImages(); ?>
    <?php foreach ($images as $image): ?>
        <img data-image-id = "<?= $image->id; ?>" src="<?= $image->getUrl('250x150'); ?>" alt="">
    <?php endforeach; ?>
    <br><br>

    <?= $form->field($model, 'square')->textInput(['maxlength' => true]) ?>

    <?php if (!Yii::$app->params['calculate'][Yii::$app->controller->id]): ?>
        <?= $form->field($model, 'projects_cost')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'renovations_cost')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'furniture_cost')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'decorations_cost')->textInput(['maxlength' => true]) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php $this->registerJS(sprintf(
        "$('[data-image-id]').on('dblclick', function(){
            var object = $(this);
            $.get('%s?id=' + $(this).data('imageId'), function(data){
                if (data) {
                    object.detach();
                }
            });
        })", Yii::$app->urlManager->createUrl(['admin/' . Yii::$app->controller->id . '/delete-image'])
    ), yii\web\View::POS_READY); ?>

</div>
