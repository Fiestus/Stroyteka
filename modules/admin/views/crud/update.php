<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Gallery */

$this->title = 'Обновить: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => $model::label(), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<section class="page">
	<br><br>
	<div class="container">
		<div class="gallery-update">

		    <h1><?= Html::encode($this->title) ?></h1>
			<br>

		    <?= $this->render('/'.Yii::$app->controller->id.'/_form', [
		        'model' => $model,
		    ]) ?>

		</div>
	</div>
	<br><br>
</section>
