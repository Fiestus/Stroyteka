<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Furniture;
use app\components\BaseController;
use yii\web\NotFoundHttpException;

class FurnitureController extends BaseController {

    public function getIndexAttributes(){
        return [
            'name',
            'description',
            'total_cost',
        ];
    }

}
