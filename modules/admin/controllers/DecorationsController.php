<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Decorations;
use app\components\BaseController;
use yii\web\NotFoundHttpException;

class DecorationsController extends BaseController {

    public function getIndexAttributes(){
        return [
            'name',
            'description',
            'total_cost',
        ];
    }

}
