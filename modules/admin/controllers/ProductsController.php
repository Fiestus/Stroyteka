<?php
namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Products;
use app\components\BaseController;
use yii\web\NotFoundHttpException;

class ProductsController extends BaseController {

    public function getIndexAttributes(){
        return [
            'id',
            'name',
            'description:html',
            'total_cost',
            'qtn',
            'article',
        ];
    }

    public function mixinView($model, $data){
        $key = array_search('category_id', $data);

        $data[$key] = [
            'value' => $model->category ? $model->category->name : 'Нет',
            'label' => 'Категория',
        ];

        $key = array_search('available', $data);
        $data[$key] = [
            'value' => $model->available ? "<span style='color:#086501;'>Да</span>" : '<span style = "color:#ff7878;">Нет</span>',
            'format'=>'html',
            'label' => $model->getAttributeLabel('available'),
        ];
        return $data;
    }
}
