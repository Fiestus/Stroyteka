<?php
    use yii\helpers\Url;
    use app\modules\admin\widgets\TreeView;
    use kartik\cmenu\ContextMenu;
?>

<li>
    <?php ContextMenu::begin(['items' => Yii::$app->controller->createContextMenu($item)]); ?>
    <span class = "name" data-category-id = "<?= $item->id ?>">
        <?= $item->name ?>
    </span>
    <?php ContextMenu::end(); ?>

    <?php if ($children = $item->children): ?>
        <ul>
        <?php foreach ($children as $child): ?>
            <?= TreeView::widget([
                'item' => $child,
            ]); ?>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</li>

<?php $this->registerJS(sprintf(
        "$('[data-create-category=$item->id]').on('click', function(){
            $.get('%s', function(data){
                $('.category-form').html('');
                $('.category-form').append(data);
            });
        });
        $('[data-edit-category=$item->id]').on('click', function(){
            $.get('%s', function(data){
                $('.category-form').html('');
                $('.category-form').append(data);
            });
        });
        ",
        Url::to([Yii::$app->controller->id . '/render-form', 'id_parent' => $item->id,]),
        Url::to([Yii::$app->controller->id . '/render-form', 'id_parent' => isset($item->parent->id) ? $item->parent->id : false, 'id' => $item->id])
    )
, yii\web\View::POS_READY); ?>
