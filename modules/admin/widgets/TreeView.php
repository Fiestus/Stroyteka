<?php

namespace app\modules\admin\widgets;

use Yii;

class TreeView extends \yii\base\Widget
{
    public $item;

    public function run()
    {
        return $this->render('treeview', [
            'item' => $this->item,
        ]);
    }
}
