<?php

namespace app\modules\admin\models;

class InteriorsRenovations extends \app\models\BaseModel
{
    public static function tableName()
    {
        return "{{%interiors_renovations}}";
    }

}
