<?php

namespace app\modules\admin\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\helpers\Helper;

class Interiors extends \app\models\Interiors
{
    public $image = false;

    public function rules(){
        return [
            [['id'], 'integer'],
            [['image'], 'file', 'maxFiles' => 10],
            [['name', 'alias', 'description', 'total_cost', 'square', 'projects_cost', 'furniture_cost', 'decorations_cost', 'renovations_cost'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = parent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'total_cost', $this->total_cost])
            ->andFilterWhere(['like', 'square', $this->square]);

        return $dataProvider;
    }

    public function beforeSave($insert){
        if (!$this->alias) {
            $this->alias = Helper::generateRandomString(8);
        }

        return parent::beforeSave($insert);
    }

    public function afterSave(){

        $relations = ['furniture', 'decorations', 'renovations'];

        foreach ($relations as $relation) {
            $modelName = "\\app\\modules\\admin\\models\\Interiors" . ucfirst($relation);
            $array = Yii::$app->request->post()['Interiors'][$relation];
            $matches = $modelName::find()->where(['interior_id' => $this->id])->all();

            foreach ($matches as $match) {
                $match->delete();
            }

            if ($array) {
                foreach ($array as $value) {
                    $model = new $modelName;
                    $model->interior_id = $this->id;
                    $model->{$relation . "_id"} = $value;
                    $model->save();
                }
            }
        }
    }

}
