<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "catalog".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $image
 * @property string $price
 * @property integer $qtn
 * @property string $article
 */
class Catalog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'image', 'price', 'qtn', 'article'], 'required'],
            [['description', 'image'], 'string'],
            [['qtn'], 'integer'],
            [['title', 'price', 'article'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'image' => 'Изображение',
            'price' => 'Цена',
            'qtn' => 'Количество',
            'article' => 'Артикул',
        ];
    }
}
