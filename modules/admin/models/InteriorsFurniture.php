<?php

namespace app\modules\admin\models;

class InteriorsFurniture extends \app\models\BaseModel
{
    public static function tableName()
    {
        return "{{%interiors_furniture}}";
    }

}
