<?php

namespace app\modules\admin\models;

use Yii;
use yii\data\ActiveDataProvider;

class Categories extends \app\models\Categories
{
    public function rules(){
        return [
            [['id'], 'integer'],
            [['name'], 'required'],
            [['id_parent', 'depth', 'name'], 'safe'],
        ];
    }

    public function beforeSave($insert)
    {
        $this->depth = ++$this->parent->depth;
        return parent::beforeSave($insert);
    }
}
