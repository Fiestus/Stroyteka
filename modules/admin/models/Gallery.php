<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "gallery".
 *
 * @property integer $id
 * @property string $title
 * @property string $totalcost
 * @property string $square
 * @property string $projectcost
 * @property string $repaircost
 * @property string $finishcost
 * @property string $furniturecost
 */
class Gallery extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'totalcost', 'square', 'projectcost', 'repaircost', 'finishcost', 'furniturecost'], 'required'],
            [['image'], 'string'],
            [['title', 'totalcost', 'square', 'projectcost', 'repaircost', 'finishcost', 'furniturecost'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'image' => 'Изображение',
            'totalcost' => 'Стоимость с мебелью и аксессуарами',
            'square' => 'Площадь объекта',
            'projectcost' => 'Общая стоимость проекта',
            'repaircost' => 'Стоимость ремонтных работ',
            'finishcost' => 'Стоимость отделочных материалов',
            'furniturecost' => 'Стоимость мебели и аксессуаров',
        ];
    }
}
