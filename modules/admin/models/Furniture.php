<?php

namespace app\modules\admin\models;

use Yii;
use yii\data\ActiveDataProvider;

class Furniture extends \app\models\Furniture
{
    public $image = false;

    public function rules(){
        return [
            [['id'], 'integer'],
            [['name', 'description', 'total_cost'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = parent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'total_cost', $this->total_cost]);

        return $dataProvider;
    }


}
