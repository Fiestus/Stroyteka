<?php

namespace app\modules\admin\models;

class InteriorsDecorations extends \app\models\BaseModel
{
    public static function tableName()
    {
        return "{{%interiors_decorations}}";
    }

}
