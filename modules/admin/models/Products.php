<?php

namespace app\modules\admin\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\helpers\Helper;

class Products extends \app\models\Products
{
    public $image = false;

    public function rules(){
        return [
            [['id'], 'integer'],
            [['name', 'available', 'alias', 'description', 'category_id', 'total_cost', 'image', 'style_type', 'article', 'qtn', 'size'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = parent::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'total_cost', $this->total_cost])
            ->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'qtn', $this->qtn]);

        return $dataProvider;
    }

    public function beforeSave($insert){
        if (!$this->alias) {
            $this->alias = Helper::generateRandomString(8);
        }

        return parent::beforeSave($insert);
    }
}
