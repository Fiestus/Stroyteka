<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'mihaildev/yii2-ckeditor' => 
  array (
    'name' => 'mihaildev/yii2-ckeditor',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@mihaildev/ckeditor' => $vendorDir . '/mihaildev/yii2-ckeditor',
    ),
  ),
  '2amigos/yii2-multi-select-widget' => 
  array (
    'name' => '2amigos/yii2-multi-select-widget',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@dosamigos/multiselect' => $vendorDir . '/2amigos/yii2-multi-select-widget',
    ),
  ),
  'costa-rico/yii2-images' => 
  array (
    'name' => 'costa-rico/yii2-images',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@rico/yii2images' => $vendorDir . '/costa-rico/yii2-images',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'execut/yii2-base' => 
  array (
    'name' => 'execut/yii2-base',
    'version' => '1.1.5.0',
    'alias' => 
    array (
      '@execut/yii' => $vendorDir . '/execut/yii2-base',
    ),
  ),
  'execut/yii2-widget-bootstraptreeview' => 
  array (
    'name' => 'execut/yii2-widget-bootstraptreeview',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@execut/widget' => $vendorDir . '/execut/yii2-widget-bootstraptreeview',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.7.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-context-menu' => 
  array (
    'name' => 'kartik-v/yii2-context-menu',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/cmenu' => $vendorDir . '/kartik-v/yii2-context-menu',
    ),
  ),
  'kartik-v/yii2-slider' => 
  array (
    'name' => 'kartik-v/yii2-slider',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/slider' => $vendorDir . '/kartik-v/yii2-slider',
    ),
  ),
);
