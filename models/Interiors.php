<?php
namespace app\models;

use app\modules\admin\models\InteriorsFurniture;
use app\modules\admin\models\InteriorsDecorations;
use app\modules\admin\models\InteriorsRenovations;

class Interiors extends BaseModel
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    public static function label(){
        return 'Галерея интерьеров';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'description' => 'Описание',
            'discount_cost' => 'Цена по скидке',
            'total_cost' => 'Цена',
            'image' => 'Изображения',
            'square' => 'Общая площадь',
            'projects_cost' => 'Общая стоимость',
            'renovations_cost' => 'Цена ремонтных работ',
            'furniture_cost' => 'Цена с мебелью',
            'decorations_cost' => 'Цена отделочных материалов',
        ];
    }

    public static function tableName(){
        return '{{%interiors}}';
    }

    public function getFurniture()
    {
        return $this->hasMany(Furniture::className(), ['id' => 'furniture_id'])
        ->viaTable(InteriorsFurniture::tableName(), ['interior_id' => 'id']);
    }

    public function getDecorations()
    {
        return $this->hasMany(Decorations::className(), ['id' => 'decorations_id'])
        ->viaTable(InteriorsDecorations::tableName(), ['interior_id' => 'id']);
    }

    public function getRenovations(){
        return $this->hasMany(Renovations::className(), ['id' => 'renovations_id'])
        ->viaTable(InteriorsRenovations::tableName(), ['interior_id' => 'id']);
    }

    public function getAllRelated()
    {
        return array_merge($this->furniture, $this->decorations, $this->renovations);
    }

    public function getAmount()
    {
        return $this->projects_cost + $this->furniture_cost + $this->decorations_cost + $this->renovations_cost;
    }
}
