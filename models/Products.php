<?php
namespace app\models;

use app\models\BaseModel;

class Products extends BaseModel
{
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ],
        ];
    }

    public static function label(){
        return 'Каталог товаров';
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'available' => 'Есть в наличии',
            'description' => 'Описание',
            'discount_cost' => 'Цена по скидке',
            'total_cost' => 'Цена',
            'image' => 'Изображения',
            'article' => 'Артикул',
            'style_type' => 'Стиль',
            'size' => 'Габариты',
        ];
    }

    public static function tableName(){
        return '{{%products}}';
    }

    public function getCategory(){
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }
}
