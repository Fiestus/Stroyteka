<?php

namespace app\models;

class Categories extends BaseModel
{
    public static function label(){
        return 'Категории';
    }

    public static function tableName(){
        return '{{%categories}}';
    }

    public function getParent()
    {
        return self::find()->andWhere(['id' => $this->id_parent])->one();
    }

    public function getChildren()
    {
        return self::find()->andWhere(['id_parent' => $this->id])->all();
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'id_parent' => 'Родительская категория',
            'depth' => 'Уровень вложенности',
        ];
    }

    public static function getList()
    {
        $list = parent::getList();
        unset($list[1]);
        return $list;
    }
}
