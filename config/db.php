<?php

return [
    'class' => 'yii\db\Connection',
    //'dsn' => 'mysql:host=localhost;dbname=stroyka',
    'dsn' => 'mysql:host=localhost;dbname=stroyteka',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_',
];
