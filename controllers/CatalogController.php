<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

use app\modules\admin\models\Catalog;

class CatalogController extends Controller
{
    public $layout = 'catalog';
    
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // 'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'index','view'
                        ],
                        'allow' => true,
                        // 'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }


    public function actionIndex()
    {
        $catalog = Catalog::find()->all();

        return $this->render('index',[
            'catalog' => $catalog,
        ]);
    }

    public function actionView()
    {
        return $this->render('view');
    }

}
